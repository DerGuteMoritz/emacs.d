;;; twittering-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (twit) "twittering-mode" "twittering-mode.el" (20854
;;;;;;  29663 0 0))
;;; Generated autoloads from twittering-mode.el

(autoload 'twit "twittering-mode" "\
Start twittering-mode.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("twittering-mode-pkg.el") (20854 29663
;;;;;;  229922 374000))

;;;***

(provide 'twittering-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; twittering-mode-autoloads.el ends here
