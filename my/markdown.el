(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))

(add-hook 'markdown-mode-hook
          (lambda ()
            (define-key markdown-mode-map
              (kbd "M-n")
              'forward-paragraph)

            (define-key markdown-mode-map
              (kbd "M-p")
              'backward-paragraph)))
