(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            (setq show-paren-delay 0)
            (paredit-mode t)))

(define-key emacs-lisp-mode-map (kbd "M-.") 'find-function-at-point)
(define-key emacs-lisp-mode-map (kbd "C-M-.") 'find-variable-at-point)
