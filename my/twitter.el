(require 'twittering-mode)
(setq twittering-use-master-password t)
(setq twittering-icon-mode t)
(setq twittering-use-native-retweet t)

(define-key twittering-mode-map
  (kbd "F") 'twittering-follow)
