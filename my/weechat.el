(setq weechat-modules '(weechat-button weechat-complete weechat-tracking))
(require 'weechat)
(setq weechat-auto-monitor-buffers t)
(setq weechat-tracking-types '(:highlight :message))
(setq weechat-tracking-show-buffer-predicate
      (lambda (message-type buffer)
        (message "top")
        (and buffer (eq :private (weechat-buffer-type buffer)))))
