(global-set-key (kbd "C-M-.") 'mark-next-like-this)
(global-set-key (kbd "C-M-,") 'mark-previous-like-this)
(global-set-key (kbd "C-M--") 'mark-all-like-this)
