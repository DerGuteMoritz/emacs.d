(defun ensure-scheme-buffer-exist ()
  (unless (get-buffer "*scheme*")
    (run-scheme)))

(defun pop-up-scheme-buffer ()
  (save-selected-window
    (select-window (display-buffer "*scheme*" t))
    (goto-char (point-max))))

(defun chicken-doc ()
  (interactive)
  (let ((func (current-word)))
    (when func
      (ensure-scheme-buffer-exist)
      (process-send-string "*scheme*" (format "(use chicken-doc) ,doc %S\n" func))
      (pop-up-scheme-buffer))))

(define-key scheme-mode-map (kbd "C-c C-d") 'chicken-doc)

(defun chicken-send-last-sexp ()
  (interactive)
  (ensure-scheme-buffer-exist)
  (scheme-send-last-sexp)
  (pop-up-scheme-buffer))

(define-key scheme-mode-map (kbd "C-x C-e") 'chicken-send-last-sexp)


(setq slime-csi-path "/home/syn/.rbenv/shims/csi")
