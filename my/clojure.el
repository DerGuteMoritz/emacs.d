(autoload 'clojure-mode "clojure-mode" "Mode for editing Clojure source files")
(add-to-list 'auto-mode-alist '("\\.clj$" . clojure-mode))
(add-to-list 'auto-mode-alist '("\\.cljs$" . clojure-mode))

(add-hook 'clojure-mode-hook
          (lambda () 
            (paredit-mode t)
            (highlight-parentheses-mode t)))

(eval-after-load 'clojure-mode
  '(define-clojure-indent
     (describe 'defun)
     (testing 'defun)
     (given 'defun)
     (using 'defun)
     (with 'defun)
     (it 'defun)
     (do-it 'defun)
     (Feature 'defun)
     (Scenario 'defun)
     (feature 'defun)
     (scenario 'defun)
     (cond-let 'defun)
     (this-as 'defun)
     (facts 'defun)
     (fact 'defun)))

;; slime
(eval-after-load 'slime
  '(progn (slime-setup '(slime-repl))))
