(require 'repository-root)
(require 'grep-o-matic)

(global-set-key (kbd "M-/") 'grep-o-matic-repository)
(global-set-key (kbd "C-M-/") 'grep-o-matic-current-directory)
(add-to-list 'repository-root-matchers repository-root-matcher/git)
