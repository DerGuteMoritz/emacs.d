(add-to-list 'auto-mode-alist '("\\.rake$" . ruby-mode))
(add-to-list 'auto-mode-alist '("/Gemfile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("/Rakefile$" . ruby-mode))

(add-hook 'ruby-mode-hook
          (lambda ()
            (define-key ruby-mode-map
              (kbd "TAB")
              'indent-and-hippie-expand)))
