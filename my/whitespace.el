;; http://p.tarn-vedra.de/toggle-show-trailing-whitespace.html

(defun toggle-show-trailing-whitespace ()
  (interactive)
  (setq show-trailing-whitespace (not show-trailing-whitespace))
  (font-lock-fontify-buffer)
  (message "Trailing whitespace %s"
           (if show-trailing-whitespace "highlighted" "hidden")))   

(global-set-key (kbd "<f7>") #'toggle-show-trailing-whitespace)
