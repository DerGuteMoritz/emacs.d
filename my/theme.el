(setq custom-theme-directory "~/.emacs.d/themes")
;; (load-theme 'wombat t  nil)
;; (load-theme 'zenburn t  nil)
(load-theme 'tango-dark)
;; (load-theme 'adwaita)
(add-to-list 'default-frame-alist
             '(font . "Terminus-12"))
