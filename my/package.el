;; requires the melpa package

;; (setq package-archive-exclude-alist '(("melpa" slime)))

(setq package-archive-exclude-alist '(("melpa" magit twilight-theme)))

