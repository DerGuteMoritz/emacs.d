;; (require 'smooth-scrolling)

(setq scroll-conservatively 1000)
(setq scroll-margin 10)
(setq scroll-preserve-screen-position t)
