(load "epresent/epresent.el")

(defvar epresent-font-faces
  '(epresent-title-face
    epresent-heading-face
    epresent-subheading-face
    epresent-author-face
    epresent-bullet-face
    epresent-hidden-face))

(defun epresent-increase-font* ()
  "Increase the presentation font size."
  (interactive)
  (dolist (face epresent-font-faces)
    (set-face-attribute face nil :height (1+ (face-attribute face :height)))))

(defun epresent-decrease-font* ()
  "Decrease the presentation font size."
  (interactive)
  (dolist (face epresent-font-faces)
    (set-face-attribute face nil :height (1- (face-attribute face :height)))))

(eval-after-load 'epresent
  '(progn
     (define-key epresent-mode-map "+" 'epresent-increase-font*)
     (define-key epresent-mode-map "-" 'epresent-decrease-font*)))
