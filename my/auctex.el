(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)

(setq TeX-engine 'xetex)
(setq TeX-PDF-mode t)

(setq TeX-view-program-list '(("zathura" "zathura %o")))

(setq TeX-view-program-selection
      '(((output-dvi style-pstricks)
         "dvips and gv")
        (output-dvi "xdvi")
        (output-pdf "zathura")
        (output-html "xdg-open")))


