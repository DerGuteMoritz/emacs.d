;;; Improve buffer naming on conflict
(require 'uniquify)
(setq uniquify-buffer-name-style 'reverse
      uniquify-separator ":")
