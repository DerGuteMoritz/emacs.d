(require 'package)

(add-to-list
 'package-archives
 '("marmalade" . "http://marmalade-repo.org/packages/") t)

(add-to-list
 'package-archives
 '("melpa" . "http://melpa.milkbox.net/packages/") t)

(package-initialize)

(require 'cl)

(add-to-list 'load-path "~/.emacs.d/lib")

(defun load-my (file)
  (load (concat "~/.emacs.d/my/" (symbol-name file))))

;; global
(load-my 'theme)
(load-my 'general)
(load-my 'auto-save)
(load-my 'scpaste)
(load-my 'hippie-expand)
(load-my 'browse-url)
(load-my 'smooth-scrolling)
(load-my 'anything)
(load-my 'printing)
(load-my 'uniquify)
(load-my 'windmove)
;; (load-my 'mark-more-like-this)
(load-my 'powerline)
(load-my 'package)
(load-my 'grep-o-matic)

;; mail
(load-my 'gnus)
(load-my 'mail)

;; modes
(load-my 'bbdb)
(load-my 'highlight-parentheses)
(load-my 'highlight-symbol)
(load-my 'magit)
(load-my 'org)
;; (load-my 'epresent)
(load-my 'ruby)
(load-my 'highline)
(load-my 'css)
(load-my 'twitter)
(load-my 'markdown)
;; (load-my 'pomodoro)
(load-my 'auctex)
(load-my 'whitespace)
(load-my 'eshell)
(load-my 'clojure-brepl)
(load-my 'electric)
(load-my 'nix)
(load-my 'weechat)
;; (load-my 'jabber)

;; ido
(load-my 'ido)
(load-my 'ido-goto-symbol)

;; lisp
(load-my 'paredit)
(load-my 'elisp)
(load-my 'scheme)
(load-my 'clojure)
(load-my 'nrepl)
;; (load-my 'slime)
;; (load-my 'geiser)

;; misc
(load-my 'keys)
(put 'erase-buffer 'disabled nil)
